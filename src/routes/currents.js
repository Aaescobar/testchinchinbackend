var express = require('express');
var router = express.Router();
var axios = require('axios')

/* GET users listing. */
router.get('/', function(req, res, next) {

  const AuthStr = 'E1DBAC4C-B7F1-47F1-A4B1-1AAF89B84D7C';
  
  
  const getCurrents = async () =>{

    await axios.get('https://rest.coinapi.io/v1/exchangerate/BTC/USD', { headers: { 'X-CoinAPI-Key': AuthStr } })
    .then(response => {
      // If request is good...
      return BTC = response.data.rate
    })
    .catch((error) => {
      console.log('error 3 ' + error);
    })
  
    await axios.get('https://rest.coinapi.io/v1/exchangerate/ETH/USD', { headers: { 'X-CoinAPI-Key': AuthStr } })
    .then(response => {
      // If request is good...
      return ETH = response.data.rate
    })
    .catch((error) => {
      console.log('error 3 ' + error);
    })
    
    await axios.get('https://rest.coinapi.io/v1/exchangerate/DASH/USD', { headers: { 'X-CoinAPI-Key': AuthStr } })
    .then(response => {
      // If request is good...
      return DASH = response.data.rate;
       
    })
    .catch((error) => {
      console.log('error 3 ' + error);
    })
  
    const PTRO = 60;
  
    const BS = 0.0001;
  
    await axios.get('https://api.exchangeratesapi.io/latest?symbols=USD')
    .then(response => {
      return EUR = response.data.rates.USD;
    })
    .catch(error =>{
      console.log('error 3 ' + error);
    })
  
    const hello = {
      rates: {
        BTC: BTC,
        ETH: ETH,
        DASH: DASH,
        PTR: PTRO,
        BS: BS,
        EUR: EUR
      }
    }    
    return res.status(200).send(hello)
  }

  return getCurrents()
  
});

module.exports = router;
